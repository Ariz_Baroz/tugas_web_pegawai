<html>
<head>
<title>Tambah Pegawai</title>
</head>
<body>
    </div>
    <br class="clear" />
  </div>
</div>
<div class="wrapper col2"></div>
<div class="wrapper col3">
  <div id="container">
    <h1 align="center">Form Tambah Pegawai</h1>
    <form method="post" action="insertpegawai.php">
<table border="1" cellpadding="5" cellspacing="0">
    <tbody>
    <tr>
    		<td>NIK</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="nik" type="text" name="nik" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Nama Lengkap</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="nama_lengkap" type="text" name="nama_lengkap" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Gelar Depan</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="gelar_depan" type="text" name="gelar_depan" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Gelar Belakang</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="gelar_belakang" type="text" name="gelar_belakang" maxlength="40" required="required" /></td>
		</tr>
        <tr>
        <td>Identitas</td>
        <td>:</td>
        <td><select name="identitas">
          <option value="ktp">KTP</option>
          <option value="sim">SIM</option>
        </select></td> 
        <tr>
    		<td>Nomor Identitas</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="nomor_identitas" type="text" name="nomor_identitas" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Tempat Lahir</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="tempat_lahir" type="text" name="tempat_lahir" maxlength="40" required="required" /></td>
		</tr>
        <tr>
        <tr>
        <td>Tanggal Lahir</td>
        <td>:</td>
        <td><input class="inputs" placeholder="tanggal_lahir" type="date" name="tanggal_lahir" maxlength="40" required="required" /></td>
    </tr>
<td>Jenis kelamin</td>
<td>:</td>
<td><select name="jenis_kelamin">
  <option value="L">Laki-laki</option>
  <option value="P">Perempuan</option>
</select></td> 
        <tr>
    		<td>Telepon</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="telepon" type="text" name="telepon" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>HP</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="hp" type="text" name="hp" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Email</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="email" type="text" name="email" maxlength="40" required="required" /></td>
		</tr>
        <tr>
    		<td>Website</td>
    		<td>:</td>
            <td><input class="inputs" placeholder="website" type="text" name="website" maxlength="40" required="required" /></td>
		</tr>
		
		
        <tr>
        <td align="right" colspan="3"><input type="reset" class="btn btn-default" name="reset"/>
      <input type="submit" name="submit" value="Simpan" /></td>
        
  </tr>
    </tbody>
</table>
</form>
</body>
</html>